// Foydalanuvchi tomonidan kiritilgan sonning yigindi va faktoriyalini aniqlab beruvchi dastur
package main

import "fmt"

func main() {

	son := 0
	fmt.Printf("Iltimos son kiriting:")
	fmt.Scanf("%d", &son)
	yigindi := 0
	faktoriyal := 1
	for i := 1; i < son; i++ {
		yigindi += i
		faktoriyal *= i
	}
	fmt.Printf("Yigindi: %d\n", yigindi)
	fmt.Printf("Faktoriyal: %d\n", faktoriyal)

}
