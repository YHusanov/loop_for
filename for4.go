  // for ni ichida sonlar qayerdan qayergacha olinishi aytiladi va "continue"bn shu sonni uqimay davomet  tashlab ket "break" shu belgilangan joyda tugatib beruvchi dastur
package main

import "fmt"

func main(){

	for i := 0; i < 10; i++{
		if i == 5{
			continue
		}else if i == 8{
			break
		}
		fmt.Println("I: ",i)
	}

	/*for i := 10; i > 5; i--{
		if i == 7{
			continue
		}else if i == 5 {
			break
		}
		fmt.Println("I: ",i)
	}*/
}