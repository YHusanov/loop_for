package main

import (
	"fmt"
	"os"
	"errors"
)

func main(){

	file, err := os.Open("example.text")
	if err != nil{
		fmt.Println("Error bor: ",err)
	}else{
		fmt.Println("Fayl: ",file.Name())
	}

	myError := errors.New("BU mening hatoim")
	fmt.Println("Errorim: ",myError)

	fayl,err := os.Open("misol.txt")
	checkError(err)
	fmt.Println("FILE: ",fayl.Name())
	
}

func checkError(err error) {
	if err != nil {
		fmt.Println("ERROR: ",err.Error())
		//LOGLASHNI AMALGA OSHIRDIK
	    os.Exit(1)
	}
}